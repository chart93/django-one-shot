from django.urls import path
from todos.views import show_todolist, show_todolist_detail, create_todolist

urlpatterns = [
    path('<int:id>/', show_todolist_detail, name='todo_list_detail'),
    path('', show_todolist, name='todo_list_list'),
    path('create/', create_todolist, name='todo_list_create'),
]
