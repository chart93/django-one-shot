from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm
# Create your views here.


def show_todolist(request):
    todolistitems = TodoList.objects.all()
    context = {
        'todo_list': todolistitems
    }
    return render(request, 'list.html', context)


def show_todolist_detail(request, id):
    todolistdetail = get_object_or_404(TodoList, id=id)
    context = {'todolistdetail': todolistdetail}
    return render(request, 'detail.html', context)


def create_todolist(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect('todolist_detail', todolist.id)
    else:
        form = TodoListForm()
        context = {
            'form': form
        }
    return render(request, 'todolists/create.html', context)
